package ru.ovechkin.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.TaskDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "app_task")
public class Task extends AbstractEntity {

    @NotNull
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    @Column(columnDefinition = "DATE", updatable = false)
    private Date creationTime = new Date(System.currentTimeMillis());

    public Task() {
    }

    @NotNull
    public Project getProject() {
        return project;
    }

    public void setProject(@NotNull Project projectEntity) {
        this.project = projectEntity;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable User userEntity) {
        this.user = userEntity;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    @Nullable
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(@Nullable Date creationTime) {
        this.creationTime = creationTime;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Nullable
    public static Task toEntity(@Nullable TaskDTO taskDTO) {
        if (taskDTO == null) return  null;
        return new Task(taskDTO);
    }

    public Task(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return;
        setId(taskDTO.getId());
        setName(taskDTO.getName());
        setDescription(taskDTO.getDescription());
    }

    @Override
    public String toString() {
        return "Task{\n" +
                "user=" + user.getLogin() +
                ",\n name='" + name + '\'' +
                ",\n description='" + description + '\'' +
                ",\n project='" + project.getName() + '\'' +
                '}';
    }

}