package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;
import ru.ovechkin.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public void add(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (projectDTO == null) return;
        @NotNull final UserDTO userDTO = context.getBean(ISessionService.class).getUser(sessionDTO);
        @NotNull final Project project = new Project(projectDTO);
        project.setUser(new User(userDTO));
        projectRepository.save(project);
    }

    @Override
    public void create(@Nullable final SessionDTO sessionDTO, @Nullable final String name) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        add(sessionDTO, projectDTO);
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        add(sessionDTO, projectDTO);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final List<Project> projects = projectRepository.findByUserId(userId);
        if (projects == null || projects.isEmpty()) return null;
        return getProjectDTOS(projects);
    }

    @Override
    public void removeAllUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        projectRepository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findProjectByUserIdAndId(userId, id);
        if (project == null) throw new ProjectUnknownException();
        return new ProjectDTO(project);
    }

    @Nullable
    @Override
    public ProjectDTO findProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findProjectByUserIdAndName(userId, name);
        if (project == null) throw new ProjectUnknownException(name);
        return new ProjectDTO(project);
    }

    @NotNull
    @Override
    public ProjectDTO updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = projectRepository.findProjectByUserIdAndId(userId, id);
        if (project == null) throw new ProjectUnknownException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return new ProjectDTO(project);
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findProjectByUserIdAndId(userId, id);
        if (project == null) throw new ProjectUnknownException();
        projectRepository.deleteProjectByUserIdAndId(userId, id);
        return new ProjectDTO(project);
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findProjectByUserIdAndName(userId, name);
        if (project == null) throw new ProjectUnknownException();
        projectRepository.deleteProjectByUserIdAndName(userId, name);
        return new ProjectDTO(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> getAllProjectsDTO() {
        @Nullable final List<Project> projectList = projectRepository.findAll();
        if (projectList == null || projectList.isEmpty()) throw new ProjectUnknownException();
        return getProjectDTOS(projectList);
    }

    @NotNull
    private List<ProjectDTO> getProjectDTOS(@NotNull List<Project> projectList) {
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            projectsDTO.add(projectDTO);
        }
        return projectsDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> loadProjects(@Nullable final List<ProjectDTO> projectsDTO) {
        if (projectsDTO == null || projectsDTO.isEmpty()) throw new ProjectUnknownException();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (@NotNull final ProjectDTO projectDTO : projectsDTO) {
            @NotNull final Project project = new Project(projectDTO);
            projectList.add(project);
        }
        projectRepository.saveAll(projectList);
        return projectsDTO;
    }

    @Override
    public void removeAllProjects() {
        projectRepository.deleteAll();
    }

}