!#/user/bin/env bash

if [ -f ./tm-server.pid ]; then
	echo "Task-manager server already started"
	exit 1;
fi

mkdir -p ../bash/logs
rm -f ../bash/logs/tm-server.log
nohup java -jar /home/roman/task-manager-27/tm-server/target/tm-server-jar-with-dependencies.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "task-manager is running with pid "$!
