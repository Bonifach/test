package ru.ovechkin.tm.exeption.empty;

public class RoleEmptyException extends RuntimeException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}