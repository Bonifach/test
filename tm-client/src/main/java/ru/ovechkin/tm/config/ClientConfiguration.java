package ru.ovechkin.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ovechkin.tm.endpoint.*;

@Configuration
@ComponentScan("ru.ovechkin.tm")
public class ClientConfiguration {

    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    public UserEndpoint userEndpoint(
            @NotNull @Autowired UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }


    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    public SessionEndpoint sessionEndpoint(
            @NotNull @Autowired SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }


    @Bean
    public StorageEndpointService storageEndpointService() {
        return new StorageEndpointService();
    }

    @Bean
    public StorageEndpoint storageEndpoint(
            @NotNull @Autowired StorageEndpointService storageEndpointService
    ) {
        return storageEndpointService.getStorageEndpointPort();
    }

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean TaskEndpoint taskEndpoint(
            @NotNull @Autowired TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}