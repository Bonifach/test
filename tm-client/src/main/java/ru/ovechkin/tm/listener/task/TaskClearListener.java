package ru.ovechkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskEndpoint;

@Component
public final class TaskClearListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    @EventListener(condition = "@taskClearListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[CLEAR TASKS]");
        taskEndpoint.removeAllTasks(sessionDTO);
        System.out.println("[OK]");
    }

}