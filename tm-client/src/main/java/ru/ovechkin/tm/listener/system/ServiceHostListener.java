package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class ServiceHostListener extends AbstractListener {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SERVER_HOST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server host";
    }

    @Override
    @EventListener(condition = "@serviceHostListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[HOST]");
        System.out.println(storageEndpoint.getServerHostInfo());
        System.out.println("[OK]");
    }

}
