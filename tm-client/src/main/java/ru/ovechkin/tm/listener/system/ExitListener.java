package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;


@Component
public final class ExitListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_EXIT;
    }

    @NotNull
    @Override
    public String description() {
        return "Close application";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.exit(0);
    }

}