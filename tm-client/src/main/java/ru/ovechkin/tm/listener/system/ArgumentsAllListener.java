package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class ArgumentsAllListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String description() {
        return "Show available arguments";
    }

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @EventListener(condition = "@argumentsAllListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        for (@NotNull final AbstractListener command : abstractListeners) {
            if (command.arg() == null || command.arg().isEmpty()) continue;
            System.out.println(command.arg());
        }
    }

}