package ru.ovechkin.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.endpoint.UserEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class UserRemoveListener extends AbstractListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user's account by login";
    }


    @Override
    @EventListener(condition = "@userRemoveListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[REMOVE USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.removeByLogin(sessionDTO, login);
        System.out.println("[OK]");
    }

}